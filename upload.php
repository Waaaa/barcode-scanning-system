<?php
//解讀條碼
function callpy($filename) {
	$pyscript = 'C:\wamp\htdocs\VSMS\scanner\readFromFile.py';
	$python = 'C:\Users\admin\AppData\Local\Programs\Python\Python37-32\python.exe';
	$file = "C:\wamp\htdocs\VSMS\scanner\images".$filename;
	//$file = "C:\wamp\htdocs\VSMS\scanner\images\barcode.png";
	$jsondata= exec("$python $pyscript $file");
	//Python回傳JSON格式的資料
	return $jsondata;
}


if($_SERVER['REQUEST_METHOD'] == "POST"){
	header('content-type: application/json');
	header('Access-Control-Allow-Origin: *');
	$img = $_POST['img'];

	//判斷是否有上傳圖片
	if (strpos($img, 'data:image/png;base64') === 0 || strpos($img, 'data:image/jpeg;base64') === 0) {
		//儲存圖片
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace('data:image/jpeg;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$filename = '\img'.date("YmdHis").'.png';
		$file = 'images'.$filename;
		if (file_put_contents($file, $data)) {
			//解讀條碼
			$res = callpy($filename);
		} else {
			$res = [
				"status" => 0,
				"data" => "",
				"msg" => "圖片未成功儲存，請稍後再試"
			];
		}
		//刪除圖片
		unlink($file);
	}else{
		$res = [
			"status" => 3,
			"data" => "",
			"msg" => "請上傳圖片"
		];
	}
	//回傳JSON格式的資料
	echo json_encode($res);
	
	exit;
}else{
	//輸出測試	
	$filename = "\barcode.png";
	echo json_encode(callpy($filename));
	exit;
	//header("HTTP/1.0 404 Not Found");
	exit;
}