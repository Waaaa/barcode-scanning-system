import cv2
import pyzbar.pyzbar as pyzbar
import os,time
import sys,io
import json

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

img = cv2.imread(sys.argv[1])
#嘗試解碼
decodedobj = pyzbar.decode(img)
data = ""
try:
    data = ""
    for obj in decodedobj:
        data = obj.data
    res = {
        "status": 1,
        "msg": "條碼判讀成功",
        "data": data.decode("utf-8")
    }

    #回傳JSON格式的資料
    print(json.dumps(res));
except Exception as e:
    res = {
        "status": 2,
        "msg": "無法判讀條碼資訊",
        "data": ""
    }
    
    #回傳JSON格式的資料
    print(json.dumps(res));


