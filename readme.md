# 簡易條碼掃描系統
可以掃一維條碼和QR Code的簡易條碼掃描系統

## 系統說明
**camera.html**  
使用JavaScript開啟使用者裝置的攝影機，將條碼拍照後取得相片傳送至後台(upload.php)判斷。

**upload.php**  
取得前端傳送的條碼照片後，儲存至伺服器的images資料夾，並取得Python程式的執行成果，回傳至前台。

**readFromFile.py**  
使用cv2及pyzbar來解讀照片中的條碼資訊，並回傳JSON格式的判讀結果。
```JSON
{
    "status": 1,
    "msg": "條碼判讀成功",
    "data": {data}
}

{
    "status": 2,
    "msg": "無法判讀條碼資訊",
    "data": ""
}
```

**readFromFileAuto.py**  
自動判讀images裡的預設測試圖片並印出判讀結果。

**readFromCam.py**  
使用Python開啟執行端的裝置相機，使用cv2及pyzbar來解讀照片中的條碼資訊並印出判讀結果。

## 開發工具與設備環境
* jquery 3.3.1
* Python 3.7.0
* PHP 7.3.5
* Apache 2.4