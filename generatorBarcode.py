import barcode
from barcode.writer import ImageWriter
code128 = barcode.get_barcode_class('code128')
itemBarcode = code128('Barcode', writer=ImageWriter());
fullname = itemBarcode.save('barcode_p')