import cv2
import pyzbar.pyzbar as pyzbar
import os,time

# 選擇第一隻攝影機
cap = cv2.VideoCapture(0)
while(True):

    ImageSN = time.strftime("%m%d%H%M%S", time.localtime())
    # 從攝影機擷取一張影像
    ret, frame = cap.read()
    # 顯示圖片
    cv2.imshow('frame', frame)
    # 若按下 y 鍵則儲存照片離開迴圈
    if cv2.waitKey(1) & 0xFF == ord('y'):
        #儲存圖片
        cv2.imwrite(ImageSN + ".jpg", frame)
        img = cv2.imread(ImageSN + ".jpg")
        #嘗試解碼
        decodedobj = pyzbar.decode(img)
        for obj in decodedobj:
            data = obj.data
            print(str(data))
        #移除剛儲存的圖片
        try:
            os.remove(ImageSN+".jpg")
            break
        except:
            pass
#釋放相機
cap.release()

# 關閉所有 OpenCV 視窗
cv2.destroyAllWindows()



