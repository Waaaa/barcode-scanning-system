import cv2
import pyzbar.pyzbar as pyzbar
import os,time

path = 'images/'
images = ['barcode.png', 'barcode_1.jpg', 'barcode_2.jpg', 'qrcode.png', 'qrcode.jpg'];
for image in images:
    print(image + "'s info: ")
    img = cv2.imread(path + image)
    #嘗試解碼
    decodedobj = pyzbar.decode(img)
    data = ""
    try:
        for obj in decodedobj:
            data = obj.data
            print(str(data) + "\n")
    except Exception as e:
        print("can't read info\n")


